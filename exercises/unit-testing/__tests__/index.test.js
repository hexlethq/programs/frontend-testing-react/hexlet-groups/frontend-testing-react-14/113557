test('Positive case.', () => {
  const src = { k: 'v', b: 'b' };
  const target = { k: 'v2', a: 'a' };
  const result = Object.assign(target, src);

  // BEGIN
  expect(result).toEqual({ k: 'v', a: 'a', b: 'b' });
  // END
});

test('Empty target, not empty source.', () => {
  const src = { a: 'a' };
  const target = {};
  const result = Object.assign(target, src);

  expect(result).toEqual({ a: 'a' });
});

test('Not empty target, empty source.', () => {
  const src = {};
  const target = { a: 'a' };
  const result = Object.assign(target, src);

  expect(result).toEqual({ a: 'a' });
});

test('Empty target, empty source.', () => {
  const src = {};
  const target = {};
  const result = Object.assign(target, src);

  expect(result).toEqual({});
});

test('Multiple sources.', () => {
  const src1 = { a: '1', b: 'b' };
  const src2 = { a: '2', c: 'c' };
  const src3 = { a: '3', d: 'd' };
  const target = { a: 'a', z: 'z' };
  const result = Object.assign(target, src1, src2, src3);

  expect(result).toEqual({
    a: '3',
    b: 'b',
    c: 'c',
    d: 'd',
    z: 'z',
  });
});

test('Nested props.', () => {
  const src = { b: { c: '1' } };
  const target = { a: 'a', b: { c: 'c' }, z: 'z' };
  const result = Object.assign(target, src);

  expect(result).toEqual({
    a: 'a',
    b: { c: '1' },
    z: 'z',
  });
});
